<?php
/**
 * bulkassign plugin for Craft CMS 3.x
 *
 * Bulk Assign Authors
 *
 * @link      kenshomedia.com
 * @copyright Copyright (c) 2020 Mr Onazi
 */

namespace kenshomedia\bulkassign\controllers;

use kenshomedia\bulkassign\Bulkassign;

use Craft;
use craft\web\Controller;
use craft\elements\Entry;

/**
 * BulkAssign Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Mr Onazi
 * @package   Bulkassign
 * @since     1.0.0
 */
class BulkAssignController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something', 'bulk-assign'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/bulkassign/bulk-assign
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the BulkAssignController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/bulkassign/bulk-assign/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $request = Craft::$app->getRequest();
        $games = $request->getRequiredParam('games');
        $author_id = $request->getRequiredParam('author');
        $site_id = $request->getRequiredParam('site');
        foreach ($games as $key => $value) {
            $entry = Entry::find()->siteId($site_id)->id($value)->one();
            $entry->authorId = $author_id;
            Craft::$app->elements->saveElement($entry);
        }
        return 'success';
    }
    public function actionBulkAssign()
    {
        $request = Craft::$app->getRequest();
        $games = $request->getRequiredParam('games');
        $author_id = $request->getRequiredParam('author');
        foreach ($games as $key => $value) {
            $entry = Entry::find()->id($value)->one();
            $entry->authorId = $author_id;
            Craft::$app->elements->saveElement($entry);
        }
        return 'success';
    }

}
