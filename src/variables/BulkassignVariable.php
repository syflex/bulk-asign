<?php
/**
 * bulkassign plugin for Craft CMS 3.x
 *
 * Bulk Assign Authors
 *
 * @link      kenshomedia.com
 * @copyright Copyright (c) 2020 Mr Onazi
 */

namespace kenshomedia\bulkassign\variables;

use kenshomedia\bulkassign\Bulkassign;

use Craft;

/**
 * bulkassign Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.bulkassign }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Mr Onazi
 * @package   Bulkassign
 * @since     1.0.0
 */
class BulkassignVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.bulkassign.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.bulkassign.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
