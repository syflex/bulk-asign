<?php
/**
 * bulkassign plugin for Craft CMS 3.x
 *
 * Bulk Assign Authors
 *
 * @link      kenshomedia.com
 * @copyright Copyright (c) 2020 Mr Onazi
 */

/**
 * bulkassign en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('bulkassign', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Mr Onazi
 * @package   Bulkassign
 * @since     1.0.0
 */
return [
    'bulkassign plugin loaded' => 'bulkassign plugin loaded',
];
